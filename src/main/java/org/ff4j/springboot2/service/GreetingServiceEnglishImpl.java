package org.ff4j.springboot2.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component("greeting.english")
public class GreetingServiceEnglishImpl implements GreetingService {

    public String sayHello(String name) {
        return "Hello " + name;
    }
}