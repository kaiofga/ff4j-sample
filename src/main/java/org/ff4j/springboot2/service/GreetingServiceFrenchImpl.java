package org.ff4j.springboot2.service;

import org.springframework.stereotype.Component;

@Component("greeting.french")
public class GreetingServiceFrenchImpl implements GreetingService {

    public String sayHello(String name) {
        return "Bonjour " + name;
    }
}