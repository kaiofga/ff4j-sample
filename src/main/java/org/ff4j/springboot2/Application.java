package org.ff4j.springboot2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main Springboot 2 application
 */
@SpringBootApplication
@ComponentScan(basePackages = {"org.ff4j.springboot2", "org.ff4j.aop"})
public class Application {
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}