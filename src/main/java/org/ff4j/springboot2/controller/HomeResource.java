package org.ff4j.springboot2.controller;

import org.ff4j.FF4j;
import org.ff4j.springboot2.service.GreetingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Home page taken from Springboot sample
 */
@RestController
public class HomeResource {

    private final FF4j ff4j;
    private final GreetingService greetingService;

    public HomeResource(FF4j ff4j,
                        GreetingService greetingService) {
        this.ff4j = ff4j;
        this.greetingService = greetingService;
    }

    @GetMapping(value = "/", produces = "text/html")
    public String sayHello() {
        StringBuilder response = new StringBuilder("<html><body><ul>");
        response.append("<li> To access the <b>FF4j Web Console</b> please go to <a href=\"./ff4j-web-console/\">/ff4j-web-console/</a>");
        response.append("<li> The max Login Attempts is: " + ff4j.getProperty("maxLoginAttempts").getValue());
        response.append("<p>Is <span style=\"color:red\">Awesome</span> feature activated ? from  ff4j.check(\"AwesomeFeature\") <span style=\"color:blue\">");
        response.append(ff4j.check("AwesomeFeature"));
        response.append("</span></body></html>");
        return response.toString();
    }

    @GetMapping(value = "/{name}", produces = "text/html")
    public String sayHello(@PathVariable("name") String name) {
        StringBuilder response = new StringBuilder("<html><body>");
        response.append("<span>");
        response.append(greetingService.sayHello(name));
        response.append("</span></body></html>");
        return response.toString();
    }

}
